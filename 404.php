<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package premier_theme
 */

get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<img src="https://media.giphy.com/media/A9EcBzd6t8DZe/giphy.gif" alt="image erreur 404">
		<section class="error-404 not-found">
			<header class="page-header">
				<h1 class="page-title"><?php esc_html_e('Oops! That page can&rsquo;t be found.', 'premier_theme'); ?></h1>
			</header><!-- .page-header -->

			<div class="page-content">
				<p><?php esc_html_e('It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'premier_theme'); ?></p>

				<?php
				get_search_form();
				$query = new WP_Query(array('posts_per_page' => 3));
				while ($query->have_posts()) :
					$query->the_post();
					// Votre boucle
				?>
					<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
					<p><?php the_excerpt(); ?></p>
				<?php  // Votre boucle fin
				endwhile;
				wp_reset_postdata();
				wp_list_pages();
				?>
			</div><!-- .page-content -->
		</section><!-- .error-404 -->

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
