<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package premier_theme
 */

get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <?php
        if (have_posts()) :
            if (is_home() && !is_front_page()) :
        ?>
                <header>
                    <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
                </header>
            <?php
            endif;
            $query = new WP_Query(array('posts_per_page' => 2, 'orderby' => 'desc',));
            while ($query->have_posts()) :
                $query->the_post();
                // Votre boucle
            ?>
                <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                <p><?php the_excerpt(); ?></p>
        <?php  // Votre boucle fin
            endwhile;
            wp_reset_postdata();
            /* Start the Loop */
            while (have_posts()) :
                the_post();

            /*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
            // get_template_part('template-parts/content', get_post_type());

            endwhile;

            the_posts_navigation();

        else :

        // get_template_part('template-parts/content', 'none');

        endif;
        ?>

    </main><!-- #main -->
</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
