<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package premier_theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<img src="wp-content\themes\premier_theme\image_entete.jpg" alt="image d'entête" class="image-entete">
	<div id="content" class="site-content">
		<nav id="site-navigation" class="main-navigation menu-sticky">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e('Primary Menu', 'premier_theme'); ?></button>
			<?php
			// affiche le logo lié à la page d'accueil
			?>
			<div class="logo">
				<?php the_custom_logo(); ?>
			</div>
			<?php bloginfo('name'); ?>
			<?php wp_nav_menu(array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
			));
			?>
		</nav><!-- #site-navigation -->
		<?php wp_body_open(); ?>
		<div id="page" class="site">
			<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'premier_theme'); ?></a>
			<header id="masthead" class="site-header">
				<div class="site-branding">
					<?php
					if (is_front_page() && is_home()) :
					?>
						<!-- esc_url permet de retourner une url propre -->
						<!-- bloginfo permet de retourner les informations sur le site -->
						<h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
					<?php
					else :
					?>
						<p class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a></p>
					<?php
					endif;
					?>
				</div><!-- .site-branding -->

			</header><!-- #masthead -->